# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-activity-pub'
  spec.version       = '0.3.3'
  spec.authors       = ['Sutty']
  spec.email         = ['sutty@riseup.net']
  spec.summary       = 'ActivityPub support for Jekyll'
  spec.homepage      = 'https://jekyll-activity-pub.sutty.nl/'
  spec.license       = 'Apache-2.0'
  spec.require_paths = %w[lib]
  spec.files         = Dir['lib/**/*.rb',
                           'LICENSE*',
                           'README*',
                           'CHANGELOG*']

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]
  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => 'https://0xacab.org/sutty/jekyll/jekyll-activity-pub',
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.required_ruby_version = Gem::Requirement.new('>= 2.7')

  spec.add_runtime_dependency 'distributed-press-api-client', '~> 0.5.0'
  spec.add_runtime_dependency 'jekyll', '~> 4'
  spec.add_runtime_dependency 'marcel', '~> 1'

  spec.add_development_dependency 'pry', '~> 0.14.0'
  spec.add_development_dependency 'rspec', '~> 3.6', '>= 3.6.0'
  spec.add_development_dependency 'rspec-tap-formatters'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'yard'
  spec.add_development_dependency 'sutty-liquid'
end
