# frozen_string_literal: true

require_relative 'jekyll-activity-pub-fep-fffd'

# Add a license to each activity by finding a page/post with layout
# "license"
Jekyll::Hooks.register :activity, :post_init, priority: 9 do |activity|
  Jekyll.logger.info 'FEP fffd:', 'Adding license'

  site = activity.site

  # Look for a page or a post
  license = site.pages.find do |page|
    page.data['layout'] == 'license'
  end

  # Find the first one in case there are several
  license ||= activity.renderer.payload['site']['posts'].find do |post|
    post.data['layout'] == 'license'
  end

  if license
    Jekyll.logger.info 'FEP fffd:', "License \"#{license.data['title']}\" found"
  else
    Jekyll.logger.info 'FEP fffd:', 'License not found, skipping...'
    next
  end

  activity.data['url'] << Jekyll::ActivityPub::Link.new(site, activity.absolute_url(license.url), 'license', 'text/html')
end
