# frozen_string_literal: true

# Implement FEP-fffd Proxy Objects by changing the url attribute to an
# array of Link objects.
#
# Priority is low so it's the last thing we do.  If you need to modify
# the original activities after this plugins runs, use priority lower
# than 10.
Jekyll::Hooks.register %i[actor activity], :post_init, priority: :low do |page|
  require 'jekyll/activity_pub/link'

  site = page.site

  Jekyll.logger.info 'FEP fffd:', 'Generating'

  page.data['url'] = [ Jekyll::ActivityPub::Link.new(site, page.data['url'], 'canonical', 'text/html') ]

  Jekyll.logger.info 'FEP fffd:', 'Generated'
end

