# frozen_string_literal: true

require 'distributed_press/v1/social/client'

require_relative 'jekyll/activity_pub'
require_relative 'jekyll/activity_pub/notifier'

# Generate Actor and WebFinger pages
Jekyll::Hooks.register :site, :post_read, priority: :low do |site|
  Jekyll::ActivityPub::Notifier.site = site

  # Private key is mandatory during build, so any errors should stop the
  # build instead of logging a warning.
  begin
    private_key = File.read(site.config['activity_pub_private_key'])
  rescue StandardError
    Jekyll.logger.warn 'ActivityPub:', 'There\'s an issue with your private key, skipping generation'

    site.config['activity_pub_disable'] = true

    next
  end

  # We only need the public key at this point
  client = DistributedPress::V1::Social::Client.new(public_key_url: nil, private_key_pem: private_key)

  site.pages << site.config['actor'] = actor = Jekyll::ActivityPub::Actor.new(site)
  site.pages << site.config['public_key'] = Jekyll::ActivityPub::PublicKey.new(site, actor, client.public_key)
  site.pages << site.config['webfinger'] = webfinger = Jekyll::ActivityPub::WebFinger.new(site, actor)
  site.pages << site.config['outbox'] = Jekyll::ActivityPub::Outbox.new(site, actor)
  site.pages << site.config['host-meta'] = Jekyll::ActivityPub::HostMeta.new(site, webfinger)
  site.pages << Jekyll::ActivityPub::Following.new(site, actor)

  # Instances
  site.config['instances'] = [Jekyll::ActivityPub::InstanceV1, Jekyll::ActivityPub::InstanceV2].map do |instance|
    instance.new(site).tap do |i|
      site.pages << i
    end
  end

  # Nodeinfo
  site.config['nodeinfos'] = [Jekyll::ActivityPub::Nodeinfo20, Jekyll::ActivityPub::Nodeinfo21].map do |nodeinfo|
    nodeinfo.new(site).tap do |n|
      site.pages << n
    end
  end

  site.pages << site.config['nodeinfo'] = Jekyll::ActivityPub::Nodeinfo.new(site, site.config['nodeinfos'])

  # Add Actor to home page
  home = site.pages.find do |page|
    page.url == '/'
  end

  if home
    Jekyll.logger.info 'ActivityPub:', 'Adding Actor to home page'
    home.data['activity'] = actor
  end
end

# Generate an activity for each document after the content has been
# rendered as HTML.
Jekyll::Hooks.register(:documents, :post_convert, priority: :high) do |doc|
  next if doc.site.config['activity_pub_disable']
  next unless doc.write?
  next if doc.data['sitemap'] == false
  next if doc.data['activity'] == false

  site = doc.site
  actor = site.config['actor']
  outbox = site.config['outbox']
  dest = doc.destination(site.dest)
  dir = Pathname.new(File.dirname(dest)).relative_path_from(site.dest).to_s

  # /slug/ => /slug.jsonld
  if (n = File.basename(dest, '.html')) == 'index'
    name = "#{File.basename(dir)}.jsonld"
    dir = File.dirname(dir)
  else
    name = "#{n}.jsonld"
  end

  Jekyll::ActivityPub::Activity.new(site, actor, doc, nil, dir, name).tap do |activity|
    doc.data['activity'] = activity

    created_at = doc.data['created_at'] || doc.date
    updated_at = doc.data['last_modified_at'] || doc.date

    create_or_update =
      if updated_at > created_at
        Jekyll::ActivityPub::Update
      else
        Jekyll::ActivityPub::Create
      end

    create_or_update.new(site, actor, activity).tap do |action|
      method = action.data['type'].downcase.to_sym
      path = activity.destination(site.dest)
      id = activity.data['id']

      Jekyll::ActivityPub::Notifier.public_send(method, path, id: id)

      outbox.data['totalItems'] += 1
      outbox.data['orderedItems'] << action

      site.config['nodeinfos'].map(&:increase_local_posts_counter!)
      site.config['instances'].each do |instance|
        instance.increase_local_posts_counter!
        instance.increase_last_status!(doc.date)
      end

      site.pages << action
    end

    site.pages << activity
  end
end

# This hook is shit because we don't have access to the site!
Jekyll::Hooks.register(:clean, :on_obsolete, priority: :low) do |list|
  next if Jekyll::ActivityPub::Notifier.site.config['activity_pub_disable']

  list.each do |path|
    Jekyll::ActivityPub::Notifier.delete path
  end
end

# Store data generated between builds and commits if
# jekyll-write-and-commit-changes is enabled.
Jekyll::Hooks.register(:site, :post_write, priority: :low) do |site|
  next if site.config['activity_pub_disable']

  Jekyll::ActivityPub::Notifier.save
end

# Paginate the outbox if needed
Jekyll::Hooks.register(:outbox, :pre_render) do |outbox|
  next if outbox.site.config['activity_pub_disable']

  outbox.order_items!
  outbox.items_to_links!
  outbox.paginate!
end
