# frozen_string_literal: true

require 'jekyll'
require 'distributed_press/v1/social/client'

module Jekyll
  module Commands
    # Send Activity Pub notifications
    class GenerateKeys < Jekyll::Command
      class << self
        def init_with_program(prog)
          prog.command(:generate_keys) do |c|
            c.syntax      'generate_keys --key-size 2048 --key path/to/privkey.pem'
            c.description 'Generate an RSA keypair for ActivityPub'

            activity_pub_private_key c

            c.option 'activity_pub_key_size', '--key-size 2048', Integer, 'RSA key size (2048 by default)'

            c.action do |_, options|
              process_with_graceful_fail(c, options, self)
            end
          end
        end

        def process(options)
          # Adjust verbosity quickly
          Jekyll.logger.adjust_verbosity(options)

          private_key_path = options['activity_pub_private_key'] || 'privkey.pem'
          key_size = options['activity_pub_key_size'] || 2048

          if File.exist? private_key_path
            raise Jekyll::Errors::FatalException, "Private key already exists: #{private_key_path}"
          end

          client = DistributedPress::V1::Social::Client.new(public_key_url: nil, key_size: key_size)

          File.open(private_key_path, 'w') do |f|
            f.write client.private_key.export
          rescue StandardError
            FileUtils.rm_f(private_key_path)
            raise
          end

          Jekyll.logger.info 'ActivityPub:', "Private key written to #{private_key_path}"
        end
      end
    end
  end
end
