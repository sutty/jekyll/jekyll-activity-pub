# frozen_string_literal: true

require 'jekyll'
require_relative '../activity_pub/notifier'

module Jekyll
  module Commands
    # Send Activity Pub notifications
    class Notify < Jekyll::Command
      class << self
        def init_with_program(prog)
          prog.command(:notify) do |c|
            c.syntax      'notify'
            c.description 'Send ActivityPub notifications'

            add_build_options(c)

            c.action do |_, options|
              process_with_graceful_fail(c, options, self)
            end
          end
        end

        # Send notifications
        #
        # @params :options [Hash]
        def process(options)
          # Adjust verbosity quickly
          Jekyll.logger.adjust_verbosity(options)

          options = configuration_from_options(options)
          site = Jekyll::Site.new(options)

          # Retrocompatibility
          if site.reader.respond_to? :read_data
            site.reader.read_data
          else
            site.data = DataReader.new(site).read(site.config['data_dir'])
          end

          Jekyll::ActivityPub::Notifier.site = site
          Jekyll::ActivityPub::Notifier.notify!
        end
      end
    end
  end
end
