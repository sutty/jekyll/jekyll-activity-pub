# frozen_string_literal: true

require 'jekyll/command'

module Jekyll
  # Extends Jekyll::Command to provide a private key to any command with
  # build steps.
  module CommandExtension
    # Add private key option
    def add_build_options(cmd)
      super

      activity_pub_private_key(cmd)
    end

    private

    def activity_pub_private_key(cmd)
      cmd.option 'activity_pub_private_key', '--key path/to/rsa.pem', String, 'Path to RSA private key in PEM format'
    end
  end
end

Jekyll::Command.singleton_class.prepend Jekyll::CommandExtension
