# frozen_string_literal: true

require 'distributed_press/v1/social/referenced_object'
require 'jekyll/activity_pub/notifier'

class DistributedPress
  module V1
    module Social
      module Liquid
        def to_liquid
          @to_liquid ||=
            begin
              if object.success?
                Jekyll::Drops::ActivityDrop.new(object: object.parsed_response)
              else
                Jekyll.logger.warn 'ActivityPub:', "Couldn't download #{object.request.uri} (Status: #{object.code})"
                {}
              end
            rescue Exception => e
              Jekyll.logger.warn 'ActivityPub:', "Couldn't download (Error: #{e.message})"
              {}
            end
        end

        def to_h
          if object.success?
            object.parsed_response.to_json
          else
            Jekyll.logger.warn 'ActivityPub:', "Couldn't download #{object.request.uri} (Status: #{object.code})"

            {}
          end
        end

        def to_s
          to_json
        end

        def to_json
          if object.success?
            object.parsed_response.to_json
          else
            Jekyll.logger.warn 'ActivityPub:', "Couldn't download #{object.request.uri} (Status: #{object.code})"

            object.request.uri
          end
        end
      end
    end
  end
end

DistributedPress::V1::Social::Reference.include DistributedPress::V1::Social::Liquid

module Jekyll
  module Drops
    # Converts an activity into a liquid drop that can be dereferenced on
    # demand.
    class ActivityDrop < Drop
      extend Forwardable

      mutable false

      # Iterate over all items of a collection
      #
      # @return [Array]
      def all_items
        return nil unless @obj.respond_to?(:each)

        @all_items ||= @obj.each.to_a.uniq { |x| x.uri }
      end

      # This means we could dereference the object, Liquid can use it to
      # check if it's possible to use it.
      def available
        true
      end

      # @todo Still no idea why Liquid/Jekyll initialize this with
      # different objects
      def initialize(obj)
        id =
          case obj
          when Jekyll::Page
            obj['id'] || obj.url
          when DistributedPress::V1::Social::ReferencedObject
            obj['id']
          when Hash
            obj[:object]['id']
          end

        raise StandardError unless id

        Jekyll.logger.info 'ActivityPub:', "Referencing #{id}"

        @original_obj = obj
        @@objects ||= {}
        @obj = @@objects[id] ||=
          case obj
          when Jekyll::Page
            DistributedPress::V1::Social::ReferencedObject.new(object: JSON.parse(obj.to_json), dereferencer: Jekyll::ActivityPub::Notifier.dereferencer)
          when DistributedPress::V1::Social::ReferencedObject
            obj
          when Hash
            obj[:object]
          else raise StandardError
          end
      rescue StandardError => e
        Jekyll.logger.warn 'ActivityPub:', "Error"
      end

      def to_s
        @obj.object.to_json
      end

      def fallback_data
        @obj
      end
    end
  end
end
