# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'
require_relative 'image'
require_relative 'property_value'
require_relative 'errors'
require_relative 'notifier'

module Jekyll
  module ActivityPub
    # Points to site's author profile
    class Actor < Jekyll::Page
      include Helper

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, base = '', dir = '', name = 'about.jsonld')
        @context = StubContext.new(registers: { site: site })
        super

        Notifier.actor_url = data['id']
        Notifier.actor = "@#{username}@#{hostname}"

        data['followers'] = Notifier.followers_url

        trigger_hooks :post_init
      end

      def read_yaml(*)
        self.data = {
          '@context' => [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1',
            {
              '@language' => locale,
              'schema' => 'http://schema.org#',
              'PropertyValue' => 'schema:PropertyValue',
              'value' => 'schema:value',
              'manuallyApprovesFollowers' => 'as:manuallyApprovesFollowers',
            }
          ],
          'type' => 'Person',
          'id' => absolute_url(url),
          'url' => site.config['url'],
          'outbox' => nil,
          'inbox' => inbox,
          'following' => nil,
          'followers' => nil,
          'preferredUsername' => username,
          'name' => public_name,
          'summary' => summary,
          'icon' => icons.first,
          'image' => images.first,
          'publicKey' => nil,
          'published' => published.xmlschema,
          'updated' => updated.xmlschema,
          'manuallyApprovesFollowers' => manually_approves_followers?,
          'attachment' => [
            PropertyValue.new(website_name, website_link)
          ]
        }
      end

      # @return [Time]
      def date
        published
      end

      private

      # By default all followers are moderated
      #
      # @return [Boolean]
      def manually_approves_followers?
        @manually_approves_followers ||=
          begin
            config = site.config.dig('activity_pub', 'manually_approves_followers')

            config.nil? ? true : config
          end
      end

      # @return [Time]
      def updated
        @updated ||= site.config.dig('activity_pub', 'updated') || site.time
      end

      # Find icons
      #
      # @return [Array]
      def icons
        @icons ||= [find_best_value_for(site.config, %w[activity_pub icons], 'logo')].flatten.compact.map do |icon|
          Image.new(site, icon, summary)
        end
      end

      # Finds the website name
      #
      # @return [String]
      def website_name
        @website_name ||= find_best_value_for(site.config, %w[activity_pub website name]) || 'Website'
      end

      def strict?
        true
      end

      def inbox
        @inbox ||=
          site.config.dig('activity_pub', 'url').dup.tap do |inbox|
            value_is_required! inbox, MissingInboxError

            inbox << '/v1/@'
            inbox << username
            inbox << '@'
            inbox << hostname
            inbox << '/inbox'
          end
      end

      # Find images
      #
      # @return [Array]
      def images
        @images ||= [find_best_value_for(site.config, %w[activity_pub images],
                                         %w[image path])].flatten.compact.map do |icon|
          Image.new(site, icon, summary)
        end
      end

      # @return [String]
      def website_link
        @website_link ||=
          <<~LINK
            <a rel="me" href="#{site.config['url']}">#{site.config['url']}</a>
          LINK
      end
    end
  end
end
