# frozen_string_literal: true

module Jekyll
  module ActivityPub
    # General error
    class Error < StandardError; end

    # The public key is missing
    class MissingPublicKeyError < Error
      def initialize(msg = 'The public key is missing, please add it to your configuration')
        super
      end
    end

    # The inbox is missing
    class MissingInboxError < Error
      def initialize(msg = 'The inbox is missing, please add it to your configuration')
        super
      end
    end

    # Notification error
    class NotificationError < Error; end
  end
end
