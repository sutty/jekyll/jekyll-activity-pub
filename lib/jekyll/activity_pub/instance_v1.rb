# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'
require_relative 'version'

module Jekyll
  module ActivityPub
    # Points to site's metadata
    class InstanceV1 < Jekyll::Page
      include Helper

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, base = '', dir = 'api/v1', name = 'instance')
        @context = StubContext.new(registers: { site: site })

        super(site, base, dir, name)

        trigger_hooks :post_init
      end

      def permalink
        'api/v1/instance/'
      end

      def output_ext
        '.json'
      end

      def read_yaml(*)
        self.data = {
          'uri' => hostname,
          'title' => public_name,
          'short_description' => summary,
          'description' => summary,
          'email' => email,
          'version' => Jekyll::ActivityPub::VERSION,
          'urls' => {},
          'stats' => {
            'user_count' => 1,
            'status_count' => 0,
            'domain_count' => 1
          },
          'thumbnail' => conditional_absolute_url(avatar),
          'languages' => languages,
          'registrations' => false,
          'approval_required' => false,
          'invites_enabled' => false,
          'configuration' => {
            'accounts' => {},
            'statuses' => {},
            'media_attachments' => {},
            'polls' => {}
          },
          'contact_account' => {
            'id' => 1,
            'username' => username,
            'acct' => username,
            'display_name' => public_name,
            'locked' => false,
            'bot' => false,
            'discoverable' => true,
            'group' => false,
            'created_at' => published.xmlschema,
            'note' => summary,
            'url' => site.config['url'],
            'uri' => site.config['url'],
            'avatar' => conditional_absolute_url(avatar),
            'avatar_static' => conditional_absolute_url(avatar),
            'header' => conditional_absolute_url(header),
            'header_static' => conditional_absolute_url(header),
            'followers_count' => 0,
            'following_count' => 0,
            'statuses_count' => 0,
            'last_status_at' => nil,
            'noindex' => false,
            'emojis' => [],
            'roles' => [],
            'fields' => []
          },
          'rules' => []
        }
      end

      def increase_local_posts_counter!
        data['stats']['status_count'] += 1
        data['contact_account']['statuses_count'] += 1
        nil
      end

      def increase_last_status!(last_status)
        self.last_status_at = last_status if !last_status_at || last_status_at < last_status
      end

      private

      def last_status_at
        data['contact_account']['last_status_at']
      end

      def last_status_at=(last_status)
        data['contact_account']['last_status_at'] = last_status
      end

      # @return [String,nil]
      def header
        @header ||= [find_best_value_for(site.config, %w[activity_pub images], %w[image path])].flatten.compact.first
      end

      # @return [Array<String>]
      def languages
        @languages ||= site.config['locales'] || [locale]
      end

      # @return [String,nil]
      def email
        @email ||= find_best_value_for(site.config, %w[activity_pub email], 'email')
      end

      def avatar
        @avatar ||= [find_best_value_for(site.config, %w[activity_pub icons], 'logo')].flatten.compact.first
      end
    end
  end
end
