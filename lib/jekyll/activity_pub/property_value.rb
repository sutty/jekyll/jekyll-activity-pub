# frozen_string_literal: true

require_relative 'helper'

module Jekyll
  module ActivityPub
    # A Key:Value map
    class PropertyValue
      include Helper

      attr_reader :data

      # @param :name [String]
      # @param :value [String]
      def initialize(name, value)
        @data = {
          'type' => 'PropertyValue',
          'name' => name.to_s,
          'value' => value.to_s
        }

        trigger_hooks :post_init
      end
    end
  end
end
