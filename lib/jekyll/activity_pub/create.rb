# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'

module Jekyll
  module ActivityPub
    # Represents a Create activity
    class Create < Jekyll::Page
      include Helper

      # Where is the date stored
      DATE_ATTRIBUTE = 'published'

      # @param :object [Jekyll::ActivityPub::Activity]
      attr_reader :object

      # @param :actor [Jekyll::ActivityPub::Actor]
      attr_reader :actor

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :actor [Jekyll::ActivityPub::Actor]
      # @param :object [Jekyll::ActivityPub::Activity]
      def initialize(site, actor, object)
        @context = StubContext.new(registers: { site: site })
        @object = object
        @actor = actor

        dest = Pathname.new(object.destination(site.dest)).relative_path_from(site.dest)
        name = "#{date.to_i}.jsonld"
        dir = File.join(
          File.dirname(dest),
          File.basename(dest, '.jsonld'),
          type.downcase
        )

        super(site, '', dir, name)

        trigger_hooks :post_init
      end

      def read_yaml(*)
        @data = {
          '@context' => 'https://www.w3.org/ns/activitystreams',
          'type' => type,
          'id' => absolute_url(url),
          'actor' => absolute_url(actor.url),
          'published' => object.data[DATE_ATTRIBUTE],
          'to' => object.data['to'],
          'cc' => object.data['cc'],
          'object' => object.data,
          'inReplyTo' => object.data['in_reply_to']
        }
      end

      # @return [Time]
      def date
        @date ||= Time.parse(object.data[DATE_ATTRIBUTE])
      end

      private

      def type
        @type ||= self.class.name.split('::').last
      end
    end
  end
end
