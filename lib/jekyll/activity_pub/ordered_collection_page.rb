# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'

module Jekyll
  module ActivityPub
    # A collection of activities
    class OrderedCollectionPage < Jekyll::Page
      include Helper

      attr_reader :outbox

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :outbox [Jekyll::ActivityPub::Outbox]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, outbox, base = '', dir = 'outbox', name = 'page.jsonld')
        @context = StubContext.new(registers: { site: site })
        @outbox = outbox

        super(site, base, dir, name)

        trigger_hooks :post_init
      end

      def read_yaml(*)
        self.data = {
          '@context' => 'https://www.w3.org/ns/activitystreams',
          'id' => absolute_url(url),
          'type' => 'OrderedCollectionPage',
          'partOf' => absolute_url(outbox.url),
          'orderedItems' => []
        }
      end
    end
  end
end
