# frozen_string_literal: true

require 'marcel'
require 'pathname'
require_relative 'helper'

module Jekyll
  module ActivityPub
    # Represents an Image
    class Image
      include Helper

      attr_reader :data

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :path [Path]
      # @param :description [String]
      def initialize(site, path, description = nil)
        @context = StubContext.new(registers: { site: site })

        @data = {
          'type' => 'Image',
          'mediaType' => Marcel::MimeType.for(Pathname.new(site.in_source_dir(path))),
          'url' => absolute_url(path),
          'name' => description.to_s
        }

        trigger_hooks :post_init
      end
    end
  end
end
