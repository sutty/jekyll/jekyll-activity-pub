# frozen_string_literal: true

require_relative 'ordered_collection'

module Jekyll
  module ActivityPub
    # An empty collection of followed actors
    class Following < OrderedCollection
      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :actor [Jekyll::ActivityPub::Actor]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, actor, base = '', dir = '', name = 'following.jsonld')
        super(site, base, dir, name)

        actor.data['following'] = absolute_url(url)

        trigger_hooks :post_init
      end
    end
  end
end
