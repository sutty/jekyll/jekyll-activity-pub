# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'
require_relative 'version'

module Jekyll
  module ActivityPub
    # Points to site's nodeinfo 2.0
    class Nodeinfo20 < Jekyll::Page
      include Helper

      def version
        '2.0'
      end

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, base = '', dir = 'nodeinfo', name = version)
        @context = StubContext.new(registers: { site: site })

        super(site, base, dir, name)

        trigger_hooks :post_init
      end

      def read_yaml(*)
        self.data = {
          'version' => version,
          'software' => {
            'name' => 'sutty-distributed-press',
            'version' => Jekyll::ActivityPub::VERSION
          },
          'protocols' => ['activitypub'],
          'services' => {},
          'openRegistrations' => false,
          'usage' => {
            'users' => {
              'total' => 1,
              'activeHalfyear' => 1,
              'activeMonth' => 1
            },
            'localPosts' => 0,
            'localComments' => 0
          },
          'metadata' => {}
        }
      end

      # The nodeinfo file is expected to be at this location always
      #
      # @return [String]
      def permalink
        "nodeinfo/#{version}.json"
      end

      # @return [Hash]
      def to_jrd
        {
          'rel' => "http://nodeinfo.diaspora.software/ns/schema/#{version}",
          'href' => absolute_url(url)
        }
      end

      # @return [nil]
      def increase_local_posts_counter!
        data['usage']['localPosts'] += 1
        nil
      end
    end
  end
end
