# frozen_string_literal: true

require_relative 'helper'

module Jekyll
  module ActivityPub
    # Represents a Link
    class Link
      include Helper

      attr_reader :data

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :href [String]
      # @param :rel [String]
      # @param :media_type [String]
      def initialize(site, href, rel = nil, media_type = nil)
        @site = site
        @data = {
          'type' => 'Link',
          'mediaType' => media_type,
          'href' => href,
          'rel' => rel
        }

        trigger_hooks :post_init
      end
    end
  end
end
