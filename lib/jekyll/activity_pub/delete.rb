# frozen_string_literal: true

require_relative 'helper'

module Jekyll
  module ActivityPub
    # Represents a Delete activity
    class Delete
      include Helper

      attr_reader :data

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :actor [Jekyll::ActivityPub::Actor]
      # @param :object [Jekyll::ActivityPub::Activity]
      def initialize(site, actor, object)
        @context = StubContext.new(registers: { site: site })

        @data = {
          '@context' => 'https://www.w3.org/ns/activitystreams',
          'id' => absolute_url(object.url).sub('.jsonld', '/delete.jsonld'),
          'type' => 'Delete',
          'actor' => absolute_url(actor.url),
          'object' => object.data['id']
        }

        trigger_hooks :post_init
      end
    end
  end
end
