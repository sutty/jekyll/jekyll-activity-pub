# frozen_string_literal: true

require 'jekyll/page_without_a_file'
require_relative 'helper'

module Jekyll
  module ActivityPub
    # Host meta
    #
    # https://www.rfc-editor.org/rfc/rfc6415
    class HostMeta < Jekyll::PageWithoutAFile
      include Helper

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, webfinger, base = '', dir = '.well-known', name = 'host-meta')
        super(site, base, dir, name)

        @webfinger = webfinger
        @context = StubContext.new(registers: { site: site })

        trigger_hooks :post_init
      end

      def permalink
        '.well-known/host-meta'
      end

      # @return [String]
      def content
        @content ||=
          <<~CONTENT
            <?xml version="1.0" encoding="UTF-8"?>
            <XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
              <Link rel="lrdd" template="#{absolute_url @webfinger.url}"/>
            </XRD>
          CONTENT
      end
    end
  end
end
