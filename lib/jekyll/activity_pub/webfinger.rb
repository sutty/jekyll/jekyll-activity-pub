# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'

module Jekyll
  module ActivityPub
    # Points to site's author
    class WebFinger < Jekyll::Page
      include Helper

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :actor [Jekyll::ActivityPub::Actor]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, actor, base = '', dir = '.well-known', name = 'webfinger')
        @context = StubContext.new(registers: { site: site })
        @actor = actor

        super(site, base, dir, name)

        # Set the actor profile in yet another format (no leading @)
        site.config['activity_pub_profile'] = "#{username}@#{hostname}"

        trigger_hooks :post_init
      end

      def read_yaml(*)
        self.data = {
          'subject' => "acct:#{username}@#{hostname}",
          'aliases' => [],
          'links' => [
            {
              'rel' => 'self',
              'type' => 'application/activity+json',
              'href' => absolute_url(@actor.url)
            },
            {
              'rel' => 'http://webfinger.net/rel/profile-page',
              'type' => 'text/html',
              'href' => site.config['url']
            }
          ]
        }.tap do |data|
          next unless @actor.data['icon']

          data['links'] <<
            {
              'rel' => 'http://webfinger.net/rel/avatar',
              'type' => @actor.data.dig('icon', 'mediaType'),
              'href' => @actor.data.dig('icon', 'url')
            }
        end
      end

      # The webfinger file is expected to be at this location always
      #
      # @return [String]
      def permalink
        '.well-known/webfinger'
      end
    end
  end
end
