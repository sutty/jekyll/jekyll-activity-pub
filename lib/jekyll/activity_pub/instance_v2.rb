# frozen_string_literal: true

require_relative 'instance_v1'

module Jekyll
  module ActivityPub
    # Points to site's metadata
    class InstanceV2 < InstanceV1
      include Helper

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, base = '', dir = 'api/v2', name = 'instance')
        super(site, base, dir, name)
      end

      def permalink
        'api/v2/instance/'
      end

      def read_yaml(*)
        self.data = {
          'domain' => hostname,
          'version' => Jekyll::ActivityPub::VERSION,
          'source_url' => 'https://0xacab.org/sutty/jekyll/jekyll-activity-pub',
          'title' => public_name,
          'description' => summary,
          'usage' => {
            'users' => {
              'active_month' => 1
            }
          },
          'thumbnail' => {
            'url' => conditional_absolute_url(avatar)
          },
          'languages' => languages,
          'configuration' => {
            'urls' => {},
            'accounts' => {},
            'statuses' => {},
            'media_attachments' => {},
            'polls' => {},
            'translation' => {},
          },
          'registrations' => {
            'enabled' => false,
            'approval_required' => false,
            'message' => nil
          },
          'contact' => {
            'email' => email,
            'account' => {
              'id' => 1,
              'username' => username,
              'acct' => username,
              'display_name' => public_name,
              'locked' => false,
              'bot' => false,
              'discoverable' => true,
              'group' => false,
              'created_at' => published.xmlschema,
              'note' => summary,
              'url' => site.config['url'],
              'avatar' => conditional_absolute_url(avatar),
              'avatar_static' => conditional_absolute_url(avatar),
              'header' => conditional_absolute_url(header),
              'header_static' => conditional_absolute_url(header),
              'followers_count' => 0,
              'following_count' => 0,
              'statuses_count' => 0,
              'last_status_at' => nil,
              'noindex' => false,
              'emojis' => [],
              'fields' => []
            },
            'rules' => []
          }
        }
      end

      def increase_local_posts_counter!
        data['contact']['account']['statuses_count'] += 1
        nil
      end

      private

      def last_status_at
        data['contact']['account']['last_status_at']
      end

      def last_status_at=(last_status)
        data['contact']['account']['last_status_at'] = last_status
      end
    end
  end
end
