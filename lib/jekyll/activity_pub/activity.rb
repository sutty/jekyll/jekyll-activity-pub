# frozen_string_literal: true

require 'jekyll/page'
require_relative 'helper'
require_relative 'document'
require_relative 'notifier'

module Jekyll
  module ActivityPub
    # Represents a post on ActivityPub
    class Activity < Jekyll::Page
      include Helper

      # HTML white space removal
      WHITE_SPACE = />\s+</.freeze

      # @param :doc [Jekyll::Document]
      attr_reader :doc

      # @param :actor [Jekyll::ActivityPub::Actor]
      attr_reader :actor

      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :actor [Jekyll::ActivityPub::Actor]
      # @param :doc [Jekyll::Document]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, actor, doc, base = '', dir = '', name = 'index.jsonld')
        @doc = doc
        @context = StubContext.new(registers: { site: site })
        @actor = actor

        super(site, base, dir, name)

        trigger_hooks :post_init
      end

      # @return [nil]
      def read_yaml(*)
        doc_content = doc.content.tr("\n", '').gsub(WHITE_SPACE, '><')

        id = absolute_url(url)

        self.data = {
          '@context' => [
            'https://www.w3.org/ns/activitystreams',
            {
              '@language' => locale,
              'sensitive' => 'as:sensitive'
            }
          ],
          'type' => 'Note',
          'id' => id,
          'url' => absolute_url(doc.url),
          'summary' => summary,
          'published' => (doc.data['created_at'] || doc.date).xmlschema,
          'updated' => doc.data['last_modified_at']&.xmlschema,
          'attributedTo' => absolute_url(actor.url),
          'to' => [
            'https://www.w3.org/ns/activitystreams#Public'
          ],
          'cc' => [Notifier.followers_url],
          'inReplyTo' => doc.data['in_reply_to'],
          'sensitive' => sensitive?,
          'content' => doc_content,
          'name' => doc.data['title'],
          'contentMap' => {
            locale => doc_content
          },
          'attachment' => attachments,
          'tag' => [],
          'replies' => Notifier.client.absolute_url(Notifier.replies(id).endpoint),
          'shares' => Notifier.client.absolute_url(Notifier.shares(id).endpoint),
          'likes' => Notifier.client.absolute_url(Notifier.likes(id).endpoint)
        }

        nil
      end

      private

      # @return [String]
      def content_warning
        @content_warning ||= doc.data['content_warning'].to_s.strip
      end

      # Find summary
      #
      # @return [String,nil]
      def summary
        @summary ||=
          if content_warning.empty?
            doc.data.slice('title', 'summary').values.join(separator)
          else
            [content_warning, doc.data['title']].join(separator)
          end
      end

      # Should it have a content warning?
      def sensitive?
        !content_warning.empty? || !!doc.data.fetch('sensitive', false)
      end

      # Separator to join title and summary by
      #
      # @return [String]
      def separator
        @separator ||= site.config.dig('activity_pub', 'separator') || ' // '
      end

      # Find attachments
      #
      # @return [Array]
      def attachments
        @attachments ||= [].tap do |array|
          case (image = doc.data['image'])
          when Hash
            array << Document.new(site, image['path'], image['description'])
          when String
            array << Document.new(site, image, summary)
          end
        end
      end
    end
  end
end
