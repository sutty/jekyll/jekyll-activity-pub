# frozen_string_literal: true

require_relative '../command_extension'
require_relative '../commands/generate_keys'
require_relative '../commands/notify'
