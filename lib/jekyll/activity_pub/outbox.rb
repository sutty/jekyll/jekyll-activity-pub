# frozen_string_literal: true

require_relative 'ordered_collection'

module Jekyll
  module ActivityPub
    # A collection of activities
    class Outbox < OrderedCollection
      # Initialize with default data
      #
      # @param :site [Jekyll::Site]
      # @param :actor [Jekyll::ActivityPub::Actor]
      # @param :base [String]
      # @param :dir [String]
      # @param :name [String]
      def initialize(site, actor, base = '', dir = '', name = 'outbox.jsonld')
        super(site, base, dir, name)

        actor.data['outbox'] = absolute_url(url)

        trigger_hooks :post_init
      end
    end
  end
end
