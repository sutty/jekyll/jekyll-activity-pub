# frozen_string_literal: true

require_relative 'activity_pub/webfinger'
require_relative 'activity_pub/actor'
require_relative 'activity_pub/activity'
require_relative 'activity_pub/outbox'
require_relative 'activity_pub/host_meta'
require_relative 'activity_pub/create'
require_relative 'activity_pub/update'
require_relative 'activity_pub/public_key'
require_relative 'activity_pub/nodeinfo'
require_relative 'activity_pub/nodeinfo_20'
require_relative 'activity_pub/nodeinfo_21'
require_relative 'activity_pub/instance_v1'
require_relative 'activity_pub/instance_v2'
require_relative 'activity_pub/following'

module Jekyll
  # ActivityPub
  module ActivityPub
  end
end
