# frozen_string_literal: true

# Converts iframes to links.  Priority is higher than high.
Jekyll::Hooks.register :activity, :post_init, priority: 31 do |activity|
  activity.data['content'].tap do |html|
    html.css('iframe[src]').each do |iframe|
      ::Nokogiri::XML::Node.new('p', html) do |p|
        ::Nokogiri::XML::Node.new('a', p) do |a|
          a['href'] = iframe['src']
          iframe.replace p
          p << a
        end
      end
    end
  end
end
