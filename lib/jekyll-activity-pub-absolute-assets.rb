# frozen_string_literal: true

require_relative 'jekyll-activity-pub-nokogiri'

# Modifies the Activity contents so any element with a src attribute
# uses absolute URLs
Jekyll::Hooks.register :activity, :post_init, priority: 35 do |activity|
  case activity
  when Jekyll::ActivityPub::Activity
    url = Addressable::URI.parse(activity.data['id'])

    activity.data['content'].css('[src]').each do |img|
      uri = Addressable::URI.parse(img['src'])

      next unless uri.relative?

      raise "Can't convert relative paths yet" if uri.path.start_with? '..'

      uri.path = "/#{uri.path}" unless uri.path.start_with? '/'

      uri.hostname = url.hostname
      uri.scheme   = url.scheme

      img['src'] = uri.to_s
    rescue Exception => e
      Jekyll.logger.error 'ActivityPub:', "#{img['src']}: #{e}"
    end
  end
end
