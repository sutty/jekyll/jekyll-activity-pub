# frozen_string_literal: true

# Implement FEP-1042 Peer to Peer Fediverse Activities by creating new
# activities and actors under supported protocols/schemes.
#
# Priority is low so it's the last thing we do.  If you need to modify
# the original activities after this plugins runs, use priority lower
# than 8.
#
# The hook runs on post render so we can add the extra pages after
# activity generation but before write.
require_relative 'jekyll-activity-pub-fep-fffd'

Jekyll::Hooks.register %i[actor activity outbox following create update delete], :pre_render, priority: 8 do |page|
  Jekyll.logger.info 'FEP 1042:', 'Generating'

  site = page.site

  # @param uri [URI,Addressable::URI]
  # @param protocol [String]
  # @return [URI,Addressable::URI]
  def add_protocol_to_uri(uri, protocol)
    uri.scheme = protocol

    if protocol != 'https' && uri.path.end_with?('.jsonld')
      uri.path = uri.path.sub('.jsonld', ".#{protocol}.jsonld")
    end

    uri
  end

  # Changes the URLs from a base URL to have a different scheme
  #
  # @param object [Any]
  # @param base_url [String]
  # @param scheme [String]
  def convert_uris(object, base_url, scheme)
    case object
    when Hash
      object.transform_values do |value|
        convert_uris(value, base_url, scheme)
      end
    when Array
      object.map do |value|
        convert_uris(value, base_url, scheme)
      end
    when String
      if object.start_with? base_url
        add_protocol_to_uri(Addressable::URI.parse(object), scheme).to_s
      else
        object
      end
    when Nokogiri::HTML5::DocumentFragment
      object.dup.tap do |html|
        html.css('[src]').each do |element|
          element['src'] = convert_uris(element['src'], base_url, scheme)
        end
      end
    when Jekyll::ActivityPub::Helper
      convert_uris(object.data, base_url, scheme)
    else
      object
    end
  end

  url = page.data['url']
  formats = ['application/ld+json; profile="https://www.w3.org/ns/activitystreams"', 'application/activity+json']
  data = page.pruned_data

  # TODO: Fetch enabled protocols from DP API
  %w[https ipns hyper bittorrent].each do |protocol|
    uri = Addressable::URI.parse(page.data['id'])
    add_protocol_to_uri(uri, protocol)

    if url
      formats.each do |format|
        url << Jekyll::ActivityPub::Link.new(site, uri.to_s, 'alternate', format)
      end
    end
  end

  %w[ipns hyper bittorrent].each do |protocol|
    uri = Addressable::URI.parse(page.data['id'])
    add_protocol_to_uri(uri, protocol)

    Jekyll.logger.debug 'FEP 1042:', "Generating #{uri.path} (#{protocol})"

    json = convert_uris(data, site.config['url'], protocol)
    json['url'] = url if url
    json['id'] = uri.to_s

    # XXX: Only works on forward slash OSes
    path = uri.path.sub(%r{\A/}, '')

    Jekyll::PageWithoutAFile.new(site, site.source, File.dirname(path), File.basename(path)).tap do |alternate|
      site.pages << alternate
      alternate.content = json.to_json
    end
  end

  Jekyll.logger.info 'FEP 1042:', 'Generated'
end
