---
title: Public key
layout: post
---

The public key informs others instances which key signs your
notifications to their inbox.

Notifications to remote inboxes are supported via Social Distributed
Press.
