---
title: Configuration
layout: post
---

All attributes for `_config.yml` and posts.

# Jekyll Configuration

```yaml
plugins:
- "jekyll-activity-pub"
- "jekyll-activity-pub-absolute-assets"
- "jekyll-activity-pub-link-iframes"
- "jekyll-activity-pub-assets-as-attachments"
- "jekyll-activity-pub-fep-fffd-distributed-press"
url: "https://examp.le"
hostname: "examp.le"
tagline: "Tagline"
description: "Description"
logo: "images/avatar.png"
locale: "en"
email: "site@examp.le"
activity_pub:
  announce: true
  url: "https://alternative.examp.le"
  hostname: "examp.le"
  username: "examp"
  email: "site@examp.le"
  public_name: "Example"
  summary: "Profile summary, uses tagline or description if missing"
  items_per_page: 20
  manually_approves_followers: false
  website:
    name: "Visit my website"
  icons:
  - "images/avatar.png"
  images:
  - "images/banner.png"
  published: 2024-01-01 00:00:00
  updated: 2024-01-01 00:00:00
```

# Posts / Pages

```yaml
---
title: "Title"
content_warning: "Content Warning"
activity: true
in_reply_to: "https://instan.ce/post/id"
created_at: 2024-01-01 00:00:00
last_modified_at: 2024-01-01 00:00:00
---

Content
```

## Alternative to `content_warning`

```yaml
---
title: "Title"
summary: "Summary"
sensitive: true
---
```
