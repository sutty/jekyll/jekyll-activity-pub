---
title: Announcements
layout: post
---

If you want to see your account announced after creating it, add
`announce: true` to the configuration.

```yaml
activity_pub:
  announce: true
```
