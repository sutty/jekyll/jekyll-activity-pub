---
title: Notifications
layout: post
---

Notifications are collected during build and sent to the Social
Inbox so they can be relayed to your followers.

They are stored along other useful information about your inbox on the
`_data/activity_pub.yml` file.

**This file should be commited to your control version system, even if
you're using a CI.**

## During build

### Create

New activities are considered created when they haven't been notified
before.

### Update

Activities are updated when their `last_modified_at` attribute is newer
than their `created_at` or `date` attributes, or the previous
`updated_at` attribute on the notifications store.

### Delete

Activities are removed when they dissapear from a previous run.  There's
not much information that can be obtained from them unless we start
digging in version control history.

### Actor

If the Actor profile changed, the profile is sent to followers for
update.

## Caching

Remote instances may apply caching, so you may not see updates right
away.
