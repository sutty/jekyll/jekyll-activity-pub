---
title: Activities
layout: post
---

Activities are ActivityPub's representation of the site's posts on the
Fediverse.

All activities are of [type
Note](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-note), but
you can change them using Hooks.

## Front matter

`title`, `summary` and `description` from the post front matter will be
used as a `summary` attribute on the Activity.

A `sensitive: true` value will use those attributes as a content
warning.

Posts with a `sitemap: false` or `activity: false` on their front matter
(or added via plugin) are skipped from generation.

Unpublished posts are also ignored, either with `published: false` front
matter or dated in the future.

The `in_reply_to` front matter can contain a URL to which the post is
a reply of, to build threads.

`created_at` and `last_modified_at` may contain timestamps and are used
to check if the activity needs to be broadcasted as updated.  Their
default values are the document date, so:

* If both are missing, the activity is always `Create` even if its other
  values change, and another instance may or may not update them.

* If `created_at` is missing, the activity will always be `Update`d
  because the default timestamp set by Jekyll is start of day.

## Configuration

There's not much to do.  This plugin will create an activity
representation for every post on your site.

## Customization

If you want to customize the activity, for instance changing its type,
or changing attributes, you'll find them on the `site.pages` collection.

See Hooks for more information.
