---
title: Site
layout: post
---

These are the changes you need to make on your site to support this
plugin.

## Configuration

If anything, this plugin will need a fully qualified HTTPS address, so
set the `url` option accordingly:

```yaml
url: "https://fully.qualified.doma.in"
```

Check the rest of the documentation for other options.

## Theme

Add the Activity alternate link to your `<head>`.  This will allow other
instances to find the Activity when people searchs your site using the
HTML version.

The Actor will be added to your home page too.

```html
<head>
  {% if page.activity %}
    <link href="{{ page.activity.id }}" rel="alternate" type="application/activity+json" />
  {% endif %}
</head>
```

Also, add the `profile:username`, so other instances can find the site's
profile.

```html
<head>
  <meta property="profile:username" content="{{ site.activity_pub_profile }}" />
</head>
```

And the Actor for website verification.

```html
<head>
  {%- if site.actor -%}
    <link rel="me" href="{{ site.actor.id }}" />
  {%- endif -%}
</head>
```
