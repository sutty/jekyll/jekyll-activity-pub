---
title: WebFinger
layout: post
---

WebFinger is used to find the ActivityPub profile for your site.

## References

- [Mastodon documentation](https://docs.joinmastodon.org/spec/webfinger/)

## Configuration

WebFinger requires a hostname and username to generate the profile.

Configuration alternatives on `_config.yml`:

```yaml
activity_pub:
  hostname: "example.org"
  username: "me"
```

Will become an account `@me@example.org`.

```yaml
hostname: "example.org"
# or
url: "https://example.org"
```

Will become `@example@example.org`.

If you use a CNAME file, this plugin will find the hostname there, so
you don't need to repeat the configuration.
