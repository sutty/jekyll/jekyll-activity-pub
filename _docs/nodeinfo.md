---
title: NodeInfo
layout: post
---

> [NodeInfo](https://github.com/jhass/nodeinfo) is an effort to create
> a standardized way of exposing metadata about a server running one of
> the distributed social networks. The two key goals are being able to
> get better insights into the user base of distributed social
> networking and the ability to build tools that allow users to choose
> the best fitting software and server for their needs.

The plugin creates a `/.well-known/nodeinfo` JRD document, and NodeInfo
2.0 and 2.1 JSON documents at `/nodeinfo/2.0.json` and
`/nodeinfo/2.1.json`.

## Webserver

Your webserver should probably be configured to serve the
`/.well-known/nodeinfo` file as an "application/jrd+json" document.

Example for Nginx:

```nginx
server {
  location = /.well-known/nodeinfo {
    default_type "application/jrd+json";
  }
}
```
