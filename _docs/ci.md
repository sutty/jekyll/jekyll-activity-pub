---
title: Building the site with a CI
layout: post
---

## Environment

Make sure environment variables contain `JEKYLL_ENV=production`.

## Private key

Private keys can be provided as secrets by your CI, but they need to be
files accessible to `jekyll`.  You can use temporary files.

## Permanent store

Notifications are tracked via a permanent store in
`_data/activity_pub.yml`.  You'll need to commit and push changes to
this file after running the CI.

If you use the `jekyll-write-and-commit-changes` plugin, the commit will
be done automatically, but `git push` needs to be run separately.

## Commands

After `jekyll build` and whatever steps you need to publish your site,
run `jekyll notify --key /path/to/private.key`.

```sh
bundle exec jekyll build --key /path/to/private.key
# push _site somewhere and wait for propagation
bundle exec jekyll notify --key /path/to/private.key
```

## Github Actions

You can generate the private key locally and set it as a base64-encoded
secret:

```sh
bundle exec jekyll generate_keys --key rsa.pem --key-size 2048
# Use xsel to send it to clipboard
base64 < rsa.pem | tr -d "\n" | xsel -ib
```

Add the secret through Github's settings. You can use [hypha.coop's
workflow](https://github.com/hyphacoop/hypha.coop/blob/ca3b4ea793752bf1f80426ec90b358b09e341db6/.github/workflows/deploy.yml#L63)
as a base. Note secrets are scrubbed from logs.
